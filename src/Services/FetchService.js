import { ApiConfig } from '../Config';
import { getState } from '../Store';
import { showErrorMessage } from '../Utils/Toast';

const responseParser = async (res) => {
    let response = null;

    try {
        response = await res.json();
        if (!(response.statusCode === 200)){
            showErrorMessage(null, response.message);
        }
    }
    catch (err) {
        response = null;
    }

    return response;
}

const errorHandler = (err) => {
    showErrorMessage(null, err.message);
    throw new Error(err);
}

const addCommonHeaders = (headers, token = null) => {

    return {
        ...headers,
        language: getState().auth.appLocale,
        authorization: `${ApiConfig.prefix} ${token}`,
    }
};

const postService = (url, body, headers, token) => (
    fetch(url, {
        method: 'POST',
        body,
        headers: addCommonHeaders(headers, token)
    }).then(responseParser).catch(errorHandler)
);

const putService = (url, body, headers) => (
    fetch(url, {
        method: 'PUT',
        body,
        headers: addCommonHeaders(headers)
    }).then(responseParser).catch(errorHandler)
);

const getService = (url, headers) => (
    fetch(url, {
        method: 'GET',
        headers: addCommonHeaders(headers)
    }).then(responseParser).catch(errorHandler)
);

const deleteService = (url, body, headers) => (
    fetch(url, {
        method: 'DELETE',
        body,
        headers: addCommonHeaders(headers)
    }).then(responseParser).catch(errorHandler)
);


export default {
    postService,
    putService,
    getService,
    deleteService,
}